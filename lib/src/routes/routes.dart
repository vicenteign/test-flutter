import 'package:flutter/material.dart';

import 'package:masup_movil/src/pages/alert_page.dart';
import 'package:masup_movil/src/pages/animated_container.dart';
import 'package:masup_movil/src/pages/avatar_page.dart';
import 'package:masup_movil/src/pages/card_page.dart';
import 'package:masup_movil/src/pages/home_page.dart';
import 'package:masup_movil/src/pages/input_page.dart';
import 'package:masup_movil/src/pages/limbo_page.dart';
import 'package:masup_movil/src/pages/movie_detail.dart';



Map<String, WidgetBuilder> getRoutes(){
  return<String, WidgetBuilder>{
        '/'                    : (BuildContext context)  => HomePage(),
        'alert'                : (BuildContext context)  => AlertPage(),
        'avatar'               : (BuildContext context)  => AvatarPage(),
        'card'                 : (BuildContext context)  => CardPage(),
        'animatedContainer'    : (BuildContext context)  => AnimatedContainerPage(),
        'input'                : (BuildContext context)  => InputPage(),
        'limbo'                : (BuildContext context)  => LimboPage(),
        'detail'              : (BuildContext context)  => MovieDetail(),


      };
}