
class Episodes {

  List<Episode> items = new List();

  Episodes();

  Episodes.fromJsonList( List<dynamic> jsonList  ) {

    if ( jsonList == null ) return;

    for ( var item in jsonList  ) {
      final episode = new Episode.fromJsonMap(item);
      items.add( episode );
    }

  }

}

class Episode {
  int id;
  String name;
  String airDate;
  String episode;
  List<dynamic> characters;
  String url;
  String created;

  Episode({
    this.id,
    this.name,
    this.airDate,
    this.episode,
    this.characters,
    this.url,
    this.created,
  
  });

  Episode.fromJsonMap( Map<String,dynamic> json ) {

    
        id          = json['id'];    
        name        = json['name']; 
        airDate     = json['air_date'];
        episode     = json['episode'];
        characters  = json['characters'];
        url         = json['url'];
        created     = json['created'];

    
 

  }


}


