
import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:masup_movil/src/models/movie_model.dart';
import 'package:masup_movil/src/models/cast_model.dart';

class MoviesService{

  String _apikey    = 'd5485c34b540c9133bb2fba1cab9220f';
  String _url       = 'api.themoviedb.org';
  String _language  = 'es-ES';


  int _trendingPage = 0  ;
  bool _loading     = false;


  List<Movie> _trending =  new List();
  final _trendingStream  = StreamController<List<Movie>>.broadcast();

  Function(List<Movie>) get trendingSink => _trendingStream.sink.add;

  Stream<List<Movie>> get trendingStream => _trendingStream.stream;

  void disposeStreams() {
    _trendingStream?.close();
  }


  Future<List<Movie>> _processReponse(Uri url) async{
    
    final resp = await http.get( url );
    final decodedData = json.decode(resp.body);

    final movies = new Movies.fromJsonList(decodedData['results']);

    return movies.items;

  }

  Future<List<Movie>> getMovies() async {

    final url = Uri.https(_url, '3/movie/now_playing',{
        'api_key'  : _apikey,
        'language' : _language,
    });
 
    return await _processReponse(url);


  }

  Future<List<Movie>> getTrending() async {

    if( _loading )  return [];
     _loading = true;
    
    _trendingPage++;

    final url  = Uri.https(_url, '3/movie/popular', {
      'api_key'   : _apikey,
      'language'  :_language,
      'page'      : _trendingPage.toString(),
    });

    final resp =  await _processReponse(url);

    _trending.addAll(resp);
    trendingSink(_trending);

    _loading = false;

    return resp;

  }

  Future<List<Actor>> getCast(String movieId) async {

    final url = Uri.https(_url, '3/movie/$movieId/credits',{
        'api_key'  : _apikey,
        'language' : _language,
    });

    final resp = await http.get(url);
    final decodedData = json.decode(resp.body);

    final cast =  new Cast.fromJsonList(decodedData['cast']);
    
    return cast.actors;
  }

}