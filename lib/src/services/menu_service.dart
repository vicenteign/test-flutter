import 'dart:io';

import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';
import 'package:http/http.dart' as http;



class _MenuService {

  List<dynamic> items =  [];
  var respues;

  _MenuService(){
    loadData();

    }
    
  
   Future <List<dynamic>> loadData()  async{

     final resp = await rootBundle.loadString('data/menu_opts.json');

        Map dataMap = json.decode(resp);  
        items = (dataMap['rutas']);

     return items;
   }

 
  
}

final menuService = new _MenuService();