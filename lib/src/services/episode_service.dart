
import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:masup_movil/src/models/episode_model.dart';

class EpisodesService{

  String _apikey    = '';
  String _url = 'rickandmortyapi.com';

  Future<List<Episode>> getEpisodes() async {


    final url = Uri.https(_url, 'api/episode');
    final resp = await http.get( url );
    final decodedData = json.decode(resp.body);

    final episodes = new Episodes.fromJsonList(decodedData['results']);


    return episodes.items;

  }



}