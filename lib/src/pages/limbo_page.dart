import 'package:flutter/material.dart';

import 'package:masup_movil/src/widgets/cardwidget.dart';
import 'package:masup_movil/src/services/movie_service.dart';
import 'package:masup_movil/src/widgets/movie_horizontal.dart';


class LimboPage extends StatefulWidget {
  @override
  _LimboPageState createState() => _LimboPageState();
}

class _LimboPageState extends State<LimboPage> {
  
  final moviesService = new MoviesService();
  
  @override
  Widget build(BuildContext context) {

    moviesService.getTrending();
        
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: Text('PopCorn app'),
        
      ),
      body: Container(
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            _swiperCards(context),
            _footer(context),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.folder_open),
          onPressed: (){
            //Navigator.pop(context);
            Navigator.pushNamed(context,'/graph');
          },
      ),
    );
  }

  Widget _swiperCards(context){

    return FutureBuilder(
      future: moviesService.getMovies(),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
        if(snapshot.hasData) {
          return CardWidget( movies : snapshot.data);
        }else{ 
          return Container(
            child: Center(
              child: CircularProgressIndicator(),
              )
          );
        }
      },
    );
  }

  Widget _footer(BuildContext context){
    
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 20.0),
            child: Text('Trending', style: Theme.of(context).textTheme.title)
          ),
          SizedBox(height: 5.0,),

          StreamBuilder(
            stream: moviesService.trendingStream,
            builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
              if(snapshot.hasData){
                return MovieHorizontal(
                   movies : snapshot.data,
                   nextPage: moviesService.getTrending,
                );
              }else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            },  
          ),
        ],
      ),
    );
  }
}