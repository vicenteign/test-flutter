import 'package:flutter/material.dart';

import 'package:masup_movil/src/models/cast_model.dart';
import 'package:masup_movil/src/models/movie_model.dart';
import 'package:masup_movil/src/services/movie_service.dart';

class MovieDetail extends StatelessWidget {


  @override
  Widget build(BuildContext context) {

  final Movie movie = ModalRoute.of(context).settings.arguments;
    
    return Scaffold(
      
      body: CustomScrollView(
          slivers: <Widget>[
            _createAppBar(movie),
            SliverList(
              delegate: SliverChildListDelegate([
                  SizedBox( height: 10.0,),
                  _bannerTitle(context, movie),
                  _movieDescription(context, movie),
                  _movieDescription(context, movie),
                  _movieDescription(context, movie),
                  _createCasting(movie),

                  
              ]),
            )
          ],
      )  
    );
  }

  Widget _createAppBar(Movie movie) {
    
    return SliverAppBar(
      elevation: 2.0,
      backgroundColor: Colors.indigoAccent.shade400,
      expandedHeight: 200.0,
      floating: false,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        title:Text(
          movie?.title,
          style:TextStyle(color: Colors.white, fontSize: 16.0)
        ),
        background: FadeInImage(
          image:NetworkImage(movie.getBackgroundImg()),
          placeholder: AssetImage('assets/jar-loading.gif'),
          fit:BoxFit.cover
        ),
      ),
    );
  }

   Widget _bannerTitle(BuildContext context, Movie movie){

     return Container(
       padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: Row(
          children: <Widget>[
            Hero(
                tag: movie.id,
                child: ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: FadeInImage(
                    image: NetworkImage(movie.getBannerImg()),
                    placeholder: AssetImage('assets/no-image.jpg'),
                    height: 150.0,
                ),
              ),
            ),
            SizedBox(width: 20.0),
            Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(movie.title, style: Theme.of(context).textTheme.title, overflow: TextOverflow.ellipsis,),
                  Text(movie.originalTitle, style: Theme.of(context).textTheme.subhead, overflow: TextOverflow.ellipsis),
                  Row(
                    children: <Widget>[
                      Icon(Icons.star_border),
                      Text(movie.voteAverage.toString(), style: Theme.of(context).textTheme.subtitle,),
                    ],
                  )
                ],

              ),
            ),
          ],
        ),
      );
   }

  Widget _movieDescription(BuildContext context, Movie movie ){

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
      child: Text(
        movie.overview,
        textAlign: TextAlign.justify,
      ),
    );
  }

  Widget _createCasting(Movie movie){
    final movieService = new MoviesService();

    return FutureBuilder(
      future: movieService.getCast(movie.id.toString()),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
        if(snapshot.hasData){
          return _createActorsPageView(snapshot.data);
        }else{
          return Center(child:CircularProgressIndicator());
        }
      },
    );
  }

  Widget _createActorsPageView(List<Actor> actors){

      return Container(
          child: SizedBox(
            height: 300.0,
            child: PageView.builder(
              pageSnapping: false,
              controller: PageController(
                viewportFraction: 0.3,
                initialPage: 1
              ),
              itemCount: actors.length,
              itemBuilder: (context, i) => _actorCard( context, actors[i] ),

            )
          ),
      );
  }

  Widget _actorCard(BuildContext context,  Actor actor){

    return Container(
      child:Column(
        children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.circular(10.0),
              child: FadeInImage(
              image:NetworkImage(actor.getProfileImg()),
              placeholder: AssetImage('assets/no-image.jpg'),
              height: 150.0,
              fit: BoxFit.cover,
              ),
            ),
        Text(actor.name, style: Theme.of(context).textTheme.subtitle, overflow: TextOverflow.ellipsis,)
        ],
      )
    );
  }
}

