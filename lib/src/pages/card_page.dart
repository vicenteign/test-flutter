import 'package:flutter/material.dart';

class CardPage extends StatelessWidget {
  @override
  Widget build (BuildContext context){
    final imgUrl1 = "https://66.media.tumblr.com/d67bf8ee7a7d9dbffe75a3c21d5373b1/tumblr_pm2j8hswKk1wjpo3zo1_500.jpg";
    final imgUrl2 = "https://66.media.tumblr.com/9a1744a6db15d494d6d502d0f0cf8b2d/tumblr_ppnt5bBL8p1v8sbgso1_1280.jpg";
    return Scaffold(
       appBar: AppBar(title: Text("How to shell marihuana online (fast)",),
       backgroundColor: Colors.green,
       ),
       body:ListView(
         padding: EdgeInsets.all(20),
         children: <Widget>[
           _cardType1(),
           SizedBox(height: 30.0,),
          _cardType3(Colors.red, imgUrl1),
           SizedBox(height: 30.0,),
          _cardType3(Colors.blue, imgUrl2),
           SizedBox(height: 30.0,),
          _cardType3(Colors.yellow, imgUrl1),

           
         ],),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.shopping_cart),
          backgroundColor: Colors.purple,
          onPressed: (){
            return null;
          },
          
        ),
      );
  }

  Widget _cardType1(){
    return Card(
      elevation: 2,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.horizontal()),
      child:Column(
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.photo_album, color: Colors.blue,),
            trailing: Icon(Icons.photo_camera),
            title: Text("Sube una foto"),
            subtitle: Text('Y añade tu cogoyo a la variedad que puedes encontrar en esta aplicación #QueTeloDije'),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FlatButton(
                child: Text(("Cancelar")),
                onPressed: () {
                },
              ),
               FlatButton(
                child: Text(("Aceptar")),
                onPressed: () {
                },
              )
            ],)
        ],
      )
    );
  }

  Widget _cardType3(Color _color, String imgUrl){
    final card = Container(
    
      child: Column(
        children: <Widget>[
          FadeInImage(
            placeholder: AssetImage('assets/jar-loading.gif'),
            fadeInDuration: Duration(milliseconds: 300),
            image:NetworkImage(imgUrl),
            fit:BoxFit.cover,
          ),
          Container(
            padding: EdgeInsets.all(20),
            child:
              Text("Los efectos producidos al fumar la White Widow se corresponden con los de las mejores plantas de dominancia sativa, ofreciendo al consumidor un colocón brutal, casi psicodélico. ",
              style:
              TextStyle(color: Colors.white), ),
            )
        ],
      ),
    );
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30.0),
        color: Colors.purple.shade400,
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: _color,
            blurRadius: 10.0,
            spreadRadius: 2.0
          )
        ]
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(30.0),
        child: card,
      ),
      
    );
  }
  
}