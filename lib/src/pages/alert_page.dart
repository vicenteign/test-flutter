import 'package:flutter/material.dart';

 
class AlertPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('alert'),
        ),
        body: Center(
          child: Container(
            child: RaisedButton(
              child: Text("Mostrar Alerta"),
              color: Colors.blue,
              shape: StadiumBorder(),
              onPressed: () => _showAlert(context)
              ,
            )
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.folder_open),
          onPressed: (){
            Navigator.pop(context);
          },
        ),
      );
  }

  void _showAlert(BuildContext context){
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context){
        return AlertDialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          title: Text("Titulo"),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text("Este es el contenido de la alerta"),
              FlutterLogo(size: 100.0,)
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text("Cancelar"),
              onPressed: ()=> Navigator.of(context).pop(),
            ),
            FlatButton(
              child: Text("Ok"),
              onPressed: ()=> Navigator.of(context).pop(),
            )
          ],
          
          );
      });
  }
}