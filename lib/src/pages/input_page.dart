import 'dart:math';

import 'package:flutter/material.dart';

class InputPage extends StatefulWidget{
  @override
  _InputPageState createState() => _InputPageState();
}


class _InputPageState extends State<InputPage>{
  
  String _nombre = "";
  String _email = "";

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text("Input page"),
      ),
      body: ListView(
        padding: EdgeInsets.all(20),
        children: <Widget>[
          _createInput(),
          Divider(),
          _createUser(),
          Divider(),
          _createMail(),
          Divider(),
          _showEmail(),
        ],)
    );
  }

  Widget _createInput(){
    return TextField(
      //autofocus: true,
      textCapitalization: TextCapitalization.words,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0)
        ),
        counter: Text('Letras: 0'),
        hintText: "Nombre de la persona",
        labelText: "Nombre",
        helperText: "Ingrese el nombre del usuario",
        suffixIcon: Icon(Icons.input),
        icon: Icon(Icons.insert_emoticon),
      
      ),
      onChanged: (value){
        _nombre = value;
        setState(() {
          
        });
    
      },
    );  
  }
  Widget _createUser(){
    return ListTile(
      title: Text("El nombre es: $_nombre")
    );
  }
   Widget _showEmail(){
    return ListTile(
      title: Text("El email es: $_email")
    );
  }
  Widget _createMail(){
    return TextField(
      //autofocus: true,
      //textCapitalization: TextCapitalization.words,
    
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        fillColor: Colors.red,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0)
        ),
        counter: Text('Letras: 0'),
        hintText: "Mail del usuario",
        labelText: "Mail",
        helperText: "Ingrese el email del usuario",
        suffixIcon: Icon(Icons.email),
        icon: Icon(Icons.email),
      
      ),
      onChanged: (value){
        _email = value;
        setState(() {
          
        });
    
      },
    );  
  }
}