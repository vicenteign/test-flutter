import 'package:flutter/material.dart';
import 'package:masup_movil/src/services/menu_service.dart';
import 'package:masup_movil/src/utils/icon_util.dart';


class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('pagina nueva'),
          backgroundColor: Colors.red.shade400,
        ),
        body: _list(),
        );
  }

  final items = ['Perro', 'Gato', 'Gallo'];

  List<Widget> _createItems() {
    List<Widget> list = new List<Widget>();
    for(var element in items){
      final tempWidget = new ListTile(
        title: Text(element),
      );
      list..add(tempWidget)
      ..add(Divider());
    }
  return list;
  }

  List<Widget> _createItemsFast(){
    return items.map((item){
      return Column(
        children: <Widget>[
          ListTile(
            title: Text(item + '!'),
            subtitle: Text("description"),
            leading: Icon(Icons.access_alarms),
            trailing: Icon(Icons.arrow_back),
            onTap: (){},

          ),
          Divider()
        ],
      );
    }).toList();

  }

  Widget _list(){

    return FutureBuilder(
      future:menuService.loadData(),
      initialData: [],
      builder: ( context, AsyncSnapshot<List<dynamic>> snapshot){

        return ListView(
          children: _listItems(snapshot.data, context),
        );
      },

    );

  
  }

  List<Widget> _listItems(List<dynamic> data, BuildContext context){
    final List<Widget> items = [];


    data.forEach((item){

      final widgetTemp = ListTile(
        title: Text( item['nombre']),
        leading: getIcon(item['icon']),
        trailing: Icon(Icons.arrow_downward),
        onTap: (){
            /*final route = MaterialPageRoute(
              builder: (context){
              return AlertPage();
              }
            );*/
            Navigator.pushNamed(context, item['ruta']);
        },);
        items..add(widgetTemp)
        ..add(Divider());

    });
        return items;

  
  
  }

}
