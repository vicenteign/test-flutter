import 'dart:math';

import 'package:flutter/material.dart';

class AnimatedContainerPage extends StatefulWidget{
  @override
  _AnimatedContainerPageState createState() => _AnimatedContainerPageState();
}



class _AnimatedContainerPageState extends State<AnimatedContainerPage>{
  
  double _width = 50.0;
  double _heigth = 50.0;
  
  Color _color = Colors.red;

  BorderRadiusGeometry _borderRadius =BorderRadius.circular(10.0);

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text("Animated Container"),
      ),
      body: Center(
        child: AnimatedContainer(
          duration: Duration(milliseconds: 200),
          curve: Curves.easeOutQuad,
          width: _width,
          height: _heigth,
          decoration: BoxDecoration(
            borderRadius: _borderRadius,
            color: _color,
          ),
        )
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.donut_large),
        onPressed: _change,
      ),
    );
  }

  void _change(){

      final random = Random();
   
        _width = random.nextInt(300).toDouble();
        _heigth = random.nextInt(300).toDouble();
        _color= Color.fromRGBO(
          random.nextInt(255),
          random.nextInt(255),
          random.nextInt(255),
          1,
         );
    setState(() {

    });
    
  }
}