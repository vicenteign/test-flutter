import 'package:flutter/material.dart';

 
class AvatarPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
        appBar: AppBar(
          title: Text('Avatar'),
          backgroundColor: Colors.redAccent.shade700,
          actions: <Widget>[
            Container(
            margin: EdgeInsets.only(right: 15.0),
            child: CircleAvatar(
              child: Text('VN'),
              backgroundColor: Colors.blue.shade900),
            ),
            Container(
            margin: EdgeInsets.only(right: 15.0),
            padding: EdgeInsets.all(5.0),
            child: CircleAvatar(
              backgroundImage: NetworkImage("https://assets.gitlab-static.net/uploads/-/system/user/avatar/3412355/avatar.png?width=90"),
            )
            )
          ],
        ),
        body: Center(
          child: Container(
            child: FadeInImage(
              image: NetworkImage("https://www.redbionova.com/wp-content/uploads/2018/11/rickmorty-770x547-c-default.jpg"),
              placeholder: AssetImage("assets/jar-loading.gif"),
              fadeInDuration: Duration(milliseconds: 200),
            )

          ),
        ),
    );
  }
}