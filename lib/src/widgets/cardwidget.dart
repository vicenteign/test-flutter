import 'package:flutter/material.dart';

import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:masup_movil/src/models/movie_model.dart';


class CardWidget extends StatelessWidget {
 
   final List<Movie> movies;

   CardWidget({ @required this.movies });


  @override
  Widget build(BuildContext context) {

   final _screenSize = MediaQuery.of(context).size;

    return Container( 
      padding: EdgeInsets.only(top: 30.0),
      child: Swiper(
          layout: SwiperLayout.STACK,
          itemWidth: _screenSize.width * 0.7,
          itemHeight: _screenSize.height * 0.5,
          itemBuilder: (BuildContext context, int index){
                return ClipRRect(
                  borderRadius: BorderRadius.circular(20.0), 
                  child: FadeInImage(
                          image: NetworkImage(movies[index].getBannerImg()),
                          placeholder: AssetImage('assets/jar-loading.gif'),
                          fit:BoxFit.cover,
                        )
                  );
          },
          itemCount: 10,
          //pagination: new SwiperPagination(),
          //control: new SwiperControl(),
      ),
    );
  }
}