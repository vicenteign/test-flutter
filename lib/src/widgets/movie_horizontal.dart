import 'package:flutter/material.dart';
import 'package:masup_movil/src/models/movie_model.dart';

class MovieHorizontal extends StatelessWidget {

  final List<Movie> movies;
  final Function nextPage;

  MovieHorizontal({ @required this.movies, this.nextPage});
    
  final _pageController = new PageController(
    initialPage: 1,
    viewportFraction: 0.3,
  );
  
  @override
  Widget build(BuildContext context) {
    
    final _screenSize = MediaQuery.of(context).size;

    _pageController.addListener(() {

      if(_pageController.position.pixels >=_pageController.position.maxScrollExtent - 200){
        nextPage();
      }

    });

    return Container(
      height: _screenSize.height * 0.2, //20% de la pantalla
      child: PageView.builder(
        pageSnapping: false,
        controller: _pageController,
        //children: _cards(context),
        itemCount: movies.length,
        itemBuilder: (context, i) => _card(context, movies[i])
        ,
      ),
      
    );
  }

  Widget _card(BuildContext context, Movie movies){

    final card = Container(
        margin: EdgeInsets.only(right: 15.0),
        child: Column(
          children: <Widget>[
            Hero(
              tag: movies.id,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: FadeInImage(
                  image: NetworkImage(movies.getBannerImg()),
                  placeholder: AssetImage('assets/jar-loading.gif'),
                  fit: BoxFit.cover,
                  height: 160.0,
                )
              ),
            ),
            //sizedBox(),
            Text(
              movies.title,
              overflow:TextOverflow.ellipsis,
              style: Theme.of(context).textTheme.caption,
            )
          ],
        ),
      );

    return GestureDetector(
      child: card,
      onTap: () {
          Navigator.pushNamed(context, 'detail', arguments:
              movies 
          );
      },
    );

  }

  List<Widget> _cards(BuildContext context){

    return movies.map( (movies) {
      
      return Container(
        margin: EdgeInsets.only(right: 15.0),
        child: Column(
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: FadeInImage(
                image: NetworkImage(movies.getBannerImg()),
                placeholder: AssetImage('assets/jar-loading.gif'),
                fit: BoxFit.cover,
                height: 160.0,
              )
            ),
            //sizedBox(),
            Text(
              movies.title,
              overflow:TextOverflow.ellipsis,
              style: Theme.of(context).textTheme.caption,
            )
          ],
        ),
      );

    }).toList();
  }
}