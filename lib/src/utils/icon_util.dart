import 'package:flutter/material.dart';

final _icons = <String, IconData>{
  'add_alert': Icons.drive_eta,
  'accessibility': Icons.accessibility,
  'folder_open': Icons.favorite,
  'donut_large': Icons.donut_large,
  'input': Icons.input,
  'limbo': Icons.account_box,
   



};

Icon getIcon(String iconName){
  return Icon(_icons[iconName], color: Colors.blue);
}