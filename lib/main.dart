import 'package:flutter/material.dart';

import 'package:masup_movil/src/pages/alert_page.dart';
import 'package:masup_movil/src/pages/avatar_page.dart';
import 'package:masup_movil/src/pages/home_page.dart';
import 'package:masup_movil/src/routes/routes.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      //home: HomePage(),
      initialRoute: '/',
      routes: getRoutes(),
      onGenerateRoute: (RouteSettings settings){

        return MaterialPageRoute(
          builder: (context) => AlertPage(),
        );
      },
    );
  }
}